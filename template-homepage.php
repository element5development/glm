<?php 
/*-------------------------------------------------------------------
    Template Name: Homepage
-------------------------------------------------------------------*/
?>
<?php get_header(); ?>

<?php get_template_part('template-parts/pages/content', 'title'); ?>

<a id="content" class="anchor"></a>

<div class="hidden-overflow">

	<?php if( !empty(get_the_content()) ) { ?>
	<section class="default-contents">
		<?php get_template_part('template-parts/pages/content', 'default'); ?>
	</section>
	<?php } ?>

	<?php if( have_rows('sections') ): ?>
	<div class="sections">
		<?php while ( have_rows('sections') ) : $i = 0; the_row(); $i++; ?>
		<?php 
					$anchor = get_sub_field('section_title');
					$anchor = strtolower( get_sub_field('section_title') );
					$anchor = str_replace(' ', '-', $anchor);
				?>
		<section class="section">
			<a id="<?php echo $anchor; ?>" class="anchor"></a>
			<span class="design-heading rellax-heading">
				<?php the_sub_field('design_section_title') ?>
			</span>
			<div class="content block">
				<h2>
					<?php the_sub_field('section_title') ?>
				</h2>
				<?php the_sub_field('content') ?>
				<?php if ( get_field('best-firm-image', 'options') && $i == 1 ) { ?>
				<div class="best-law-firm">
					<div class="block">
						<?php $image = get_field('best-firm-image', 'options'); ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<h2>
							<?php the_field('best-firm-heading', 'options'); ?>
						</h2>
						<p>
							<?php the_field('best-firm-description', 'options'); ?>
						</p>
					</div>
				</div>
				<?php } ?>
			</div>
			<?php	$images = get_sub_field('images'); ?>
				<?php if( $images ): ?>
				<div class="gallery block">
					<?php foreach( $images as $image ): ?>
					<img class="rellax-image" src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endforeach; ?>
				</div>
				<?php endif; ?>
		</section>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>

	<div class="gov-and-fax">
		<section class="fax-feed feed default-contents">
			<h2>Latest Garan Report</h2>
			<?php //QUERY 2 LAW FAX
				$args = array( 
					'posts_per_page'  => 2, 
					'post_type' => 'publication',
					'tax_query' => array(
						array (
								'taxonomy' => 'law',
								'field' => 'garan-report',
								'terms' => '8',
						)
					),
				);
				$publication_gov_query = new WP_Query( $args );
			?>
			<?php if ( $publication_gov_query->have_posts() ) : ?>
			<?php while ( $publication_gov_query->have_posts() ) : $publication_gov_query->the_post(); ?>
			<?php get_template_part( 'template-parts/posts/previews/preview-publication' ); ?>
			<?php endwhile; ?>
			<?php endif; ?>
			<a href="<? echo get_site_url(); ?>/law/law-fax" class="button">View All</a>
		</section>
		<section class="gov-feed feed default-contents">
			<h2>Latest Legal Talk</h2>
			<?php //QUERY 2 PUBLICATIONS
				$args = array( 
					'posts_per_page'  => 2, 
					'post_type' => 'publication',
					'tax_query' => array(
						array (
								'taxonomy' => 'law',
								'field' => 'legal-talk',
								'terms' => '9',
						)
					),
				);
				$publication_gov_query = new WP_Query( $args );
			?>
			<?php if ( $publication_gov_query->have_posts() ) : ?>
			<?php while ( $publication_gov_query->have_posts() ) : $publication_gov_query->the_post(); ?>
			<?php get_template_part( 'template-parts/posts/previews/preview-publication' ); ?>
			<?php endwhile; ?>
			<?php endif; ?>
			<a href="<? echo get_site_url(); ?>/law/gov-law" class="button">View All</a>
		</section>
	</div>

	<?php // get_template_part('template-parts/elements/ask-attorney'); ?>

	<?php // get_template_part('template-parts/elements/testimonies'); ?>

</div>

<?php get_footer(); ?>