<?php 
/*-------------------------------------------------------------------
    Template Name: Thank You
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<a id="content" class="anchor"></a>

<section class="thank-title-section title-section">
	<div class="thank-message">
		<h1>Success!</h1>
		<h2><?php the_field('thank_you_message'); ?></h2>
		<svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
			<path d="M42.473 18.817c-2.32-.15-5.723.05-9.325.396 1.036-4.208 2.417-10.05 2.91-12.773.79-4.308-5.328-5.842-6.66-2.476-1.333 3.367-5.33 14.803-12.83 16.883-.345.1-.74.297-.74.297l-.05 23.02s14.656 3.07 23.192 3.07c4.293 0 7.204-.742 7.648-4.85.592-5.497 1.283-12.428 1.58-16.487.394-5.15-1.185-6.783-5.725-7.08zM10.058 19.647H3.684c-1.405 0-2.51 1.104-2.51 2.51v21.93c0 1.406 1.105 2.51 2.51 2.51h6.374c1.405 0 2.51-1.104 2.51-2.51v-21.93c0-1.406-1.155-2.51-2.51-2.51z" fill-rule="nonzero"/>
		</svg>
	</div>
</section>

<?php get_footer(); ?>