<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

	<?php get_template_part('template-parts/posts/content', 'title'); ?>

	<a id="content" class="anchor"></a>

	<div class="sidebar-and-content">

		<?php if( !empty(get_the_content()) ) { ?>
			<section class="default-contents">
				<h1><?php the_field('content_heading'); ?></h1>
				<?php $posts = get_field('author'); ?>
				<?php if( $posts ): ?>
					<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
						<?php setup_postdata($post); ?>
						<h4><?php the_field('title'); ?></h4>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				<?php endif; ?>
				<?php get_template_part('template-parts/pages/content', 'default'); ?>
			</section>
		<?php } ?>

		<aside class="publication-sidebar">
			<div class="publication-sidebar-inner">
				<?php $posts = get_field('author'); ?>
				<?php if( $posts ): ?>
					<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
						<div class="author">
							<?php setup_postdata($post); ?>
							<?php 
								$image = get_field('headshot'); 
								$medImage = $image['sizes'][ 'medium' ];
							?>
							<a href="<?php the_permalink(); ?>">
								<img src="<?php echo $medImage; ?>" alt="<?php the_field('title'); ?>" />
								<h2><?php the_field('title'); ?></h2>
							</a>
							<?php while ( have_rows('titles') ) : the_row(); ?>
								<h4><?php the_sub_field('title'); ?></h4>
							<?php endwhile; ?>
							<div class="contact">
								<?php if ( get_field('email') ) : ?>
									<a href="mailto:<?php the_field('email'); ?>">
										<svg width="23" height="15" viewBox="0 0 23 15">
											<defs>
												<path id="a" d="M264 1245.83h19.47v-12.64H264z"/>
												<path id="b" d="M264 1233.19l9.73 8.65 9.74-8.65"/>
											</defs>
											<use fill-opacity="0" stroke="#fff" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" xlink:href="#a" transform="translate(-262 -1232)"/>
											<use fill-opacity="0" stroke="#fff" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" xlink:href="#b" transform="translate(-262 -1232)"/>
										</svg>
									</a>
								<?php endif; ?>
								<?php if ( get_field('phone_number') ) : ?>
									<?php $phone = preg_replace('/[^0-9]/', '', get_field('phone_number'));; ?>
									<a href="tel:+1<?php echo $phone; ?>">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="23" height="23" viewBox="0 0 23 23">
											<defs>
												<path id="c" d="M340.11 1241.6a1.6 1.6 0 0 0-2.17.43c-.78.97-1.75 2.58-5.4-1.08-3.65-3.65-2.04-4.61-1.08-5.4a1.6 1.6 0 0 0 .43-2.16c-.27-.4-1.97-3.03-2.27-3.46-.3-.44-.7-1.15-1.63-1.01-.7.1-3.34 1.5-3.34 4.49 0 2.98 2.35 6.65 5.57 9.87 3.22 3.21 6.88 5.57 9.87 5.57 2.98 0 4.39-2.65 4.49-3.34.13-.94-.58-1.34-1.01-1.64-.44-.3-3.06-2-3.46-2.26z"/>
											</defs>
											<use fill-opacity="0" stroke="#fff" stroke-miterlimit="50" stroke-width="2" xlink:href="#c" transform="translate(-323 -1227)"/>
										</svg>
									</a>
								<?php endif; ?>
								<?php
									$post_object = get_field('location');
									if( $post_object ): 
										$post = $post_object;
										setup_postdata( $post ); 
								?>
									<p><?php the_title(); ?></p>
									<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
								<?php endif; ?>
							</div>
						</div>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				<?php endif; ?>
			</div>
		</aside>

	</div>

<?php get_footer(); ?>