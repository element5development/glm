<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/content', 'title'); ?>

<a id="content" class="anchor"></a>

<div class="gov-and-fax">
	<section class="fax-feed feed default-contents">
		<h2>The Garan Report</h2>
		<?php //QUERY 3 EVENTS
			$args = array( 
				'posts_per_page'  => 3, 
				'post_type' => 'publication',
				'tax_query' => array(
					array (
							'taxonomy' => 'law',
							'field' => 'garan-report',
							'terms' => '8',
					)
				),
			);
			$publication_gov_query = new WP_Query( $args );
		?>
		<?php if ( $publication_gov_query->have_posts() ) : ?>
		<?php while ( $publication_gov_query->have_posts() ) : $publication_gov_query->the_post(); ?>
		<?php get_template_part( 'template-parts/posts/previews/preview-publication' ); ?>
		<?php endwhile; ?>
		<?php endif; ?>
		<a href="<? echo get_site_url(); ?>/law/garan-report/" class="button">View All</a>
	</section>
	<section class="gov-feed feed default-contents">
		<h2>Legal Talk</h2>
		<?php //QUERY 3 EVENTS
			$args = array( 
				'posts_per_page'  => 3, 
				'post_type' => 'publication',
				'tax_query' => array(
					array (
							'taxonomy' => 'law',
							'field' => 'legal-talk',
							'terms' => '9',
					)
				),
			);
			$publication_gov_query = new WP_Query( $args );
		?>
		<?php if ( $publication_gov_query->have_posts() ) : ?>
		<?php while ( $publication_gov_query->have_posts() ) : $publication_gov_query->the_post(); ?>
		<?php get_template_part( 'template-parts/posts/previews/preview-publication' ); ?>
		<?php endwhile; ?>
		<?php endif; ?>
		<a href="<? echo get_site_url(); ?>/law/legal-talk/" class="button">View All</a>
	</section>
</div>

<?php get_template_part( 'template-parts/elements/no-fault-manual' ); ?>

<?php get_template_part( 'template-parts/elements/best-law-firm' ); ?>

<?php get_footer(); ?>