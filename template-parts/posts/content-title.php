<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php //GET BACKGROUND IMAGE
	if ( get_field('title_background_image') ) {
		$title_background_array = get_field('title_background_image');
		$title_background = $title_background_array['url'];
	} else {
		$title_background = get_stylesheet_directory_uri() . '/dist/images/default-page-banner.jpg';
	}
?>

<section class="post-title title-section" style="background-image: url(<?php echo $title_background; ?>);">

	<div class="block">
		<div class="block">
			
			<?php if ( 'post' == get_post_type() || 'publication' == get_post_type() ) { //POST DATE  ?>
				<p class="post-date"><?php the_date('F d, Y'); ?></p>
			<?php } else if ( 'events' == get_post_type() ) { ?>
				<p class="post-date">Upcoming Event</p>
			<?php } else if ( 'attorney' == get_post_type() ) { ?>
				<?php
					$post_object = get_field('location');
					if( $post_object ): 
						$post = $post_object;
						setup_postdata( $post ); 
				?>
					<p class="post-date"><?php the_title(); ?></p>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				<?php endif; ?>
			<?php } else if ( 'openposition' == get_post_type() ) { ?>
				<p class="post-date"><?php the_field('location'); ?></p>
			<?php } ?>
			

			<?php if ( 'publication' == get_post_type() && has_term( 'law-fax', 'law' ) ) { //LAW FAX PUBLICATION ?>
				<h1><span>Vol. <?php the_field('roman_numeral'); ?></span>Law Fax<span>No. <?php the_field('number'); ?></span></h1>
			<?php } else if ( 'publication' == get_post_type() && has_term( 'gov-law', 'law' ) ) { //GOV LAW PUBLICATION ?>
				<h1>Gov Law</h1>
			<?php } else if ( get_field('title') ) { //POST TITLE ?>
				<h1><?php the_field('title'); ?></h1>
			<?php } else { ?>
				<h1><?php the_title(); ?></h1>
			<?php } ?>

			<?php if ( 'post' == get_post_type() ) { //POST CATEGORIES ?>
				<p class="post-cat">
					Category: <?php echo get_the_category_list(', ') ?>
				</p>
			<?php } else if ( 'events' == get_post_type() ) { ?>
				<?php if ( get_field('start_date') ) : ?>
					<p class="post-cat"><?php the_field('start_date'); ?> | <?php the_field('start_time'); ?> - <?php the_field('end_time'); ?> | <?php the_field('location'); ?></p>
				<?php else : ?>
					<p class="post-cat"><?php the_field('sub-heading'); ?></p>
				<?php endif; ?>
			<?php } else if ( 'attorney' == get_post_type() ) { ?>
				<?php while ( have_rows('titles') ) : the_row(); ?>
					<h3><?php the_sub_field('title'); ?></h3>
				<?php endwhile; ?>
				<div class="button-contain">
					<?php if ( get_field('email') ) : ?>
						<a href="mailto:<?php the_field('email'); ?>" class="button">Email</a>
					<?php endif; ?>
					<?php if ( get_field('phone_number') ) : ?>
						<?php $phone = preg_replace('/[^0-9]/', '', get_field('phone_number'));; ?>
						<a href="tel:+1<?php echo $phone; ?>" class="button">Call</a>
					<?php endif; ?>
				</div>
			<?php } else if ( 'publication' == get_post_type() ) { ?>
				<?php $posts = get_field('editor'); ?>
				<?php if( $posts ): ?>
					<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
							<?php setup_postdata($post); ?>
							<?php 
								$image = get_field('headshot_thumb'); 
								$thumbImage = $image['sizes'][ 'thumbnail' ];
							?>
							<a href="<?php the_permalink(); ?>">
								<img src="<?php echo $thumbImage; ?>" alt="<?php the_field('title'); ?>" />
							</a>
							<p class="post-cat"><a href="<?php the_permalink(); ?>"><?php the_field('title'); ?>, Editor</a></p>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				<?php endif; ?>
			<?php } ?>
		</div>
	</div>

	<div class="overlay"></div>

</section>