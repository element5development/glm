<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php //QUERY ALL LOCATIONS
	$args = array( 
		'posts_per_page'  => -1, 
		'post_type' => 'openposition',
		);
	$position_query = new WP_Query( $args );
?>
<?php if ( $position_query->have_posts() ) { ?>
<table class="positions default-contents">
	<tr>
		<th>Position</th>
		<th>Location</th>
	</tr>
	<?php while ( $position_query->have_posts() ) : $position_query->the_post(); ?>
	<tr>
		<td>
			<a href="<?php the_permalink(); ?>">
				<?php the_title(); ?>
			</a>
		</td>
		<td>
			<?php the_field('location'); ?>
		</td>
	</tr>
	<?php endwhile; ?>
</table>
<?php } else  { ?>
<p>There are currently no open positions.</p>
<?php } ?>
<?php wp_reset_query(); ?>