<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php //GET BACKGROUND IMAGE
	if ( get_field('title_background_image') ) {
		$title_background_array = get_field('title_background_image');
		$title_background = $title_background_array['url'];
	} else {
		$title_background = get_stylesheet_directory_uri() . '/dist/images/default-page-banner.jpg';
	}
?>

<article class="news-preview post-preview">
	<a href="<?php the_permalink(); ?>">
		<div class="heading" style="background-image: url(<?php echo $title_background; ?>);">
			<?php if ( get_field('title') ) { //POST TITLE ?>
				<h3><?php the_field('title'); ?></h3>
			<?php } else { ?>
				<h3><?php the_title(); ?></h3>
			<?php } ?>
			<div class="overlay"></div>
		</div>
		<div class="excerpt">
			<p><?php echo get_excerpt(125); ?></p>
		</div>
		<div class="meta">
			<p><?php the_date('M d, Y'); ?></p>
		</div>
	</a>
</article>