<?php if ( has_term( 'garan-report', 'law' ) ) { ?>

	<article class="publication-preview post-preview">
		<a href="<?php the_permalink(); ?>">
			<div class="heading">
				<p>Garan Report Volume <?php the_field('roman_numeral'); ?></p>
				<p>No. <?php the_field('number'); ?></p>
			</div>
			<div class="excerpt">
				<p><?php echo get_excerpt(150); ?></p>
			</div>
		</a>
		<div class="meta">
			<?php $publication_post = get_field('author'); ?>
			<?php if( $publication_post ): $i = 0; ?>
				<?php foreach( $publication_post as $author): // variable must NOT be called $post (IMPORTANT) ?>
					<?php 
						$i++;
						$image = get_field('headshot_thumb', $author->ID); 
						$thumbImage = $image['sizes'][ 'thumbnail' ];
					?>
					<?php if ( $i < 2 ) : ?>
						<img src="<?php echo $thumbImage; ?>" alt="<?php the_field('title', $author->ID); ?>" />
						<p>By <a href="<?php the_permalink($author->ID); ?>"><?php the_field('title', $author->ID); ?></a></p>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
			<p><?php the_date('M d, Y'); ?></p>
		</div>
	</article>

<?php } else { ?>

	<article class="publication-preview post-preview">
		<a href="<?php the_permalink(); ?>">
			<div class="heading">
				<p>Legal Talk</p>
				<p><?php the_date('F Y'); ?></p>
			</div>
			<div class="excerpt">
				<p><?php echo get_excerpt(150); ?></p>
			</div>
		</a>
		<div class="meta">
			<?php $publication_post = get_field('author'); ?>
			<?php if( $publication_post ): ?>
				<?php foreach( $publication_post as $author): // variable must NOT be called $post (IMPORTANT) ?>
					<?php 
						$image = get_field('headshot_thumb', $author->ID); 
						$thumbImage = $image['sizes'][ 'thumbnail' ];
					?>
					<img src="<?php echo $thumbImage; ?>" alt="<?php the_field('title',$author->ID); ?>" />
					<p>By <a href="<?php the_permalink($author->ID); ?>"><?php the_field('title',$author->ID); ?></a></p>
				<?php endforeach; ?>
			<?php endif; ?>
			<p><?php the_date('M d, Y'); ?></p>
		</div>
	</article>

<?php } ?>