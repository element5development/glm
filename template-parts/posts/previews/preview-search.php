<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<article class="search-preview post-preview">
		<a href="<?php the_permalink(); ?>">
			<div class="heading">
				<?php if ( get_field('title') ) { ?>
					<p><?php the_field('title'); ?></p>
				<?php } else { ?>
					<p><?php the_title(); ?></p>
				<?php } ?>
			</div>
			<div class="excerpt">
				<p><?php echo get_excerpt(150); ?></p>
			</div>
		</a>
		<div class="meta">
			<a href="<?php the_permalink(); ?>">
				<img src="<?php echo  get_stylesheet_directory_uri(); ?>/dist/images//next-arrow.svg" alt="read more" />
				<?php if ( 'post' == get_post_type() || 'publication' == get_post_type() ) { ?>
					<p><?php the_date('M d, Y'); ?></p>
				<?php } ?>
			</a>
		</div>
	</article>