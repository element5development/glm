<article class="attorney-preview post-preview">
	<a href="<?php the_permalink(); ?>">
		<div class="overlay"></div>
		<?php 
			$image = get_field('headshot'); 
			$medImage = $image['sizes'][ 'medium' ];
		?>
		<div class="headshot" style="background-image: url(<?php echo $medImage; ?>)"></div>
		<div class="info">
			<?php 
				if ( get_field('title') ) { 
					$name = get_field('title');
				} else {
					$name = get_the_title();
				}
				$lastSpacePosition = strrpos($name, ' ');
				$firstName = substr($name, 0, $lastSpacePosition);
				$lastNameArray = explode(' ', $name);
				$lastName = array_pop($lastNameArray);
			?>
			<h2><?php echo $firstName; ?><br/><?php echo $lastName; ?></h2>
			<?php
				$post_object = get_field('location');
				if( $post_object ): 
					$post = $post_object;
					setup_postdata( $post ); 
			?>
				<h3><?php the_title(); ?></h3>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>
		</div>
	</a>
</article>