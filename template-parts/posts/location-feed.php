<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php //QUERY ALL LOCATIONS
	$args = array( 
		'posts_per_page'  => -1, 
		'post_type' => 'location',
		);
	$location_query = new WP_Query( $args );
?>
<?php if ( $location_query->have_posts() ) : ?>
	<section class="post-feed feed default-contents">
		<?php while ( $location_query->have_posts() ) : $location_query->the_post(); ?>
			<?php get_template_part( 'template-parts/posts/previews/preview-location' ); ?>
		<?php endwhile; ?>
	</section>
<?php endif; ?>
<?php wp_reset_query(); ?>