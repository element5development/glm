<section class="newsletter">
	<div class="block">
		<h2>Stay in the know</h2>
		<p>Fill out the form below to be the first to hear about news and events.</p>
		<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
	</div>
</section>