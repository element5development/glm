<?php if ( get_field('best-firm-image', 'options') ) { ?>
	<section class="best-law-firm">
		<div class="block">
			<?php $image = get_field('best-firm-image', 'options'); ?>
			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<h2><?php the_field('best-firm-heading', 'options'); ?></h2>
			<p><?php the_field('best-firm-description', 'options'); ?></p>
		</div>
	</section>
<?php } ?>