
<?php //QUERY 2 PUBLICATIONS
	$args = array( 
		'posts_per_page'  => -1, 
		'post_type' => 'testimony',
	);
	$publication_gov_query = new WP_Query( $args );
?>
<?php if ( $publication_gov_query->have_posts() ) : ?>
<section class="testimonies">
	<h2>What's keeping us up at night:</h2>
	<div class="testimony-sldier">
	<?php while ( $publication_gov_query->have_posts() ) : $publication_gov_query->the_post(); ?>
		<a href="<?php the_field('link'); ?>">
			<article class="testimony-preview post-preview">
				<?php 
					$image = get_field('headshot'); 
					$thumb = $image['sizes'][ 'thumbnail' ];
				?>
				<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />
				<blockquote>
					<p><?php the_field('quote'); ?></p>
					<p><?php the_field('name'); ?></p>
				</blockquote>
			</article>
		</a>
	<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>