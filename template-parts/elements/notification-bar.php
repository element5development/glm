<?php 
/*----------------------------------------------------------------*\

	NOTIFICATION BAR

\*----------------------------------------------------------------*/
?>
<?php if ( get_field('notification_bar_text', 'options') ) : ?>
<div class="notification-bar">
	<div> 
		<p><?php the_field('notification_bar_text', 'options'); ?><?php $notificationlink = get_field('notification_bar_link', 'options'); if( $notificationlink ): ?> <a href="<?php echo $notificationlink['url']; ?>"><?php echo $notificationlink['title']; ?></a><?php endif; ?>.</p>
	</div>
</div>
<?php endif; ?>