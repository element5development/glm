<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<div class="copyright">
	<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?> All rights reserved.</p>
	<nav class="footer-nav">
		<?php if (has_nav_menu('footer_nav')) :
			wp_nav_menu(['theme_location' => 'footer_nav', 'menu_class' => 'nav']);
		endif; ?>
	</nav>
</div>
<div id="element5-credit">
	<a target="_blank" href="https://element5digital.com">
		<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" />
	</a>
</div>