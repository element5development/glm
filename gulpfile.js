/*----------------------------------------------------------------*\
	Gulp.js configuration
\*----------------------------------------------------------------*/
'use strict';
/*------------------------------*\
	source and build folders
\*------------------------------*/
const dir = {
	src: 'assets/',
	build: 'dist/'
};
const images = {
	src: dir.src + 'images/**/*',
	build: dir.build + 'images/'
};
const videos = {
	src: dir.src + 'videos/**/*',
	build: dir.build + 'videos/'
};
const fonts = {
	src: dir.src + 'fonts/**/*',
	build: dir.build + 'fonts/'
}
const scripts = {
	src: dir.src + 'scripts/**/*',
}
/*------------------------------*\
	gulp and plugins
\*------------------------------*/
const gulp = require('gulp');
const newer = require('gulp-newer');
const imagemin = require('gulp-imagemin');
const postcss = require('gulp-postcss');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

/*----------------------------------------------------------------*\
	image processing
\*----------------------------------------------------------------*/
gulp.task('images', () => {
	return gulp.src(images.src)
		.pipe(newer(images.build))
		.pipe(imagemin())
		.pipe(gulp.dest(images.build));
});
/*----------------------------------------------------------------*\
	video processing
\*----------------------------------------------------------------*/
gulp.task('videos', () => {
	return gulp.src(videos.src)
		.pipe(newer(videos.build))
		.pipe(gulp.dest(videos.build));
});
/*----------------------------------------------------------------*\
	font processing
\*----------------------------------------------------------------*/
gulp.task('fonts', () => {
	return gulp.src(fonts.src)
		.pipe(newer(fonts.build))
		.pipe(gulp.dest(fonts.build));
});
/*----------------------------------------------------------------*\
	css settings
\*----------------------------------------------------------------*/
var css = {
	src: dir.src + 'styles/main.css',
	watch: dir.src + 'styles/**/*',
	build: dir.build + 'styles/',
	processors: [
		require('postcss-assets')({
			loadPaths: ['images/'],
			basePath: dir.build,
			baseUrl: 'dist/styles'
		}),
		require("postcss-import"),
		require('postcss-nesting')({}),
		require('postcss-custom-media')({}),
		require('autoprefixer'),
		require('cssnano')
	]
};
/*----------------------------------------------------------------*\
	css processing
\*----------------------------------------------------------------*/
gulp.task('css', gulp.series(['images', 'videos', 'fonts'], () => {
	return gulp.src(css.src)
		.pipe(postcss(css.processors))
		.pipe(gulp.dest(css.build))
}));
/*----------------------------------------------------------------*\
	vendor js settings
\*----------------------------------------------------------------*/
const jsVendors = {
	src: dir.src + 'scripts/vendors/**/*',
	build: dir.build + 'scripts/vendors/',
	filename: 'vendors.js'
}
/*----------------------------------------------------------------*\
	vendor js processing
\*----------------------------------------------------------------*/
gulp.task('jsVendors', () => {
	return gulp.src(jsVendors.src)
		.pipe(concat(jsVendors.filename))
		.pipe(uglify())
		.pipe(gulp.dest(jsVendors.build))
});
/*----------------------------------------------------------------*\
	custom js settings
\*----------------------------------------------------------------*/
const js = {
	src: dir.src + 'scripts/master/**/*',
	build: dir.build + 'scripts/master/',
	filename: 'main.js'
};
/*----------------------------------------------------------------*\
	custom js processing
\*----------------------------------------------------------------*/
gulp.task('js', gulp.series(['jsVendors'], () => {
	return gulp.src(js.src)
		.pipe(concat(js.filename))
		.pipe(uglify())
		.pipe(gulp.dest(js.build))
}));
/*----------------------------------------------------------------*\
	all functions once
\*----------------------------------------------------------------*/
gulp.task('build', gulp.series(['css', 'js']));
/*----------------------------------------------------------------*\
	watch for changes
\*----------------------------------------------------------------*/
gulp.task('watch', () => {
	gulp.watch(images.src, gulp.series('images'));
	gulp.watch(videos.src, gulp.series('videos'));
	gulp.watch(fonts.src, gulp.series('fonts'));
	gulp.watch(css.watch, gulp.series('css'));
	gulp.watch(scripts.src, gulp.series('js'));
});
/*----------------------------------------------------------------*\
	run & then
\*----------------------------------------------------------------*/
gulp.task('default', gulp.series(['build', 'watch']));