<?php

/*-----------------------------------------
		MENUS - www.wp-hasty.com
-----------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_left_nav' => __( 'Primary Left Navigation'),
		'primary_right_nav' => __( 'Primary Right Navigation'),
		'footer_nav' => __( 'Footer Navigation'),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );