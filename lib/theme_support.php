<?php

/*-----------------------------------------
  INCLUDE MAIN CSS AND JS
-----------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );

  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.1', 'all');

	wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array (), 1.1, true);
	
	wp_enqueue_script('maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD2r2zJcnu-nCt5BHzAWlXPN7NB4Z5wufw', array (), 1.1, true);

	wp_enqueue_script('mainjs', get_template_directory_uri() . '/dist/scripts/master/main.js', array ( 'jquery' ), 1.1, true);

}
add_action('wp_enqueue_scripts', 'wp_main_assets');


/*-----------------------------------------
  Enable Post Thumbnails
-----------------------------------------*/
add_theme_support('post-thumbnails');

/*-----------------------------------------
  Excerpts
-----------------------------------------*/
add_post_type_support( 'page', 'excerpt' );
add_post_type_support( 'post', 'excerpt' );

/*-----------------------------------------
  Enable Post Formats
-----------------------------------------*/
// add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

/*-----------------------------------------
  Enable HTML5 Markup Support
-----------------------------------------*/
add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

/*-----------------------------------------
  SEO Yoast Breadcrumbs
-----------------------------------------*/
add_theme_support( 'yoast-seo-breadcrumbs' );

function jb_crumble_bread($link_text, $id) {
	$link_text = html_entity_decode($link_text);
	$crumb_length = strlen( $link_text );
 	$crumb_size = 14;
 	$crumble = substr( $link_text, 0, $crumb_size );
	if ( $crumb_length > $crumb_size ) {
		$crumble .= '...';
	}
	return $crumble;
}
add_filter('wp_seo_get_bc_title', 'jb_crumble_bread', 10, 2);

/*-----------------------------------------
  Use Main Stylesheet for Visual Editor
-----------------------------------------*/
add_editor_style('/assets/styles/main.css');

/*-----------------------------------------
  EXCERPT CHARACTER LENGTH
-----------------------------------------*/
function get_excerpt($limit, $source = null){
	if($source == "content" ? ($excerpt = get_the_content()) : ($excerpt = get_the_excerpt()));
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $limit);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'...';
	return $excerpt;
}

/*-----------------------------------------
  SEARCH POST COUNT
-----------------------------------------*/
function change_wp_search_size($query) {
	if ( $query->is_search ) // Make sure it is a search page
		$query->query_vars['posts_per_page'] = 10; // Change 10 to the number of posts you would like to show

	return $query; // Return our modified query variables
}
add_filter('pre_get_posts', 'change_wp_search_size'); // Hook our custom function onto the request filter
