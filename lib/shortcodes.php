<?php

/*-----------------------------------------
  BUTTON SHORTCODE
-----------------------------------------*/
function cta_button($atts, $content = null) {
  extract( shortcode_atts( array(
    'target' => '',
    'url' => '#',
		'type' => '',
		'size' => '',
		'anchor' => '',
  ), $atts ) );
  $link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$type.' is-'.$size.' '.$anchor.'">' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode('btn', 'cta_button');

/*-----------------------------------------
  OPEN POSITIONS
-----------------------------------------*/
function get_positions($atts) {
  ob_start();
  get_template_part('template-parts/posts/positions-feed');
  return ob_get_clean();
}
add_shortcode('positions', 'get_positions');