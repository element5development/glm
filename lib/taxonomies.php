<?php

/*-----------------------------------------
	CUSTOM TAXONOMIES - www.wp-hasty.com
-----------------------------------------*/
// Taxonomy Key: law
function create_law_tax() {
	$labels = array(
		'name'              => _x( 'Law', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Law', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Law', 'textdomain' ),
		'all_items'         => __( 'All Law', 'textdomain' ),
		'parent_item'       => __( 'Parent Law', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Law:', 'textdomain' ),
		'edit_item'         => __( 'Edit Law', 'textdomain' ),
		'update_item'       => __( 'Update Law', 'textdomain' ),
		'add_new_item'      => __( 'Add New Law', 'textdomain' ),
		'new_item_name'     => __( 'New Law Name', 'textdomain' ),
		'menu_name'         => __( 'Law', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
	);
	register_taxonomy( 'law', array('publication', ), $args );
}
add_action( 'init', 'create_law_tax' );
