<?php

/*-----------------------------------------
  ENABLE ACF OPTIONS PAGE
-----------------------------------------*/
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page();
}

/*-----------------------------------------
		AUTO CLOSE ACF SECTIONS
-----------------------------------------*/
function my_acf_admin_head() {
 ?>
 <script type="text/javascript">
   (function($){
     $(document).ready(function(){
       $('.layout').addClass('-collapsed');
       $('.acf-postbox').addClass('closed');
     });
   })(jQuery);
 </script>
 <?php
}
add_action('acf/input/admin_head', 'my_acf_admin_head');

/*-----------------------------------------
		GOOGLE MAPS
-----------------------------------------*/
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyD2r2zJcnu-nCt5BHzAWlXPN7NB4Z5wufw');
}
add_action('acf/init', 'my_acf_init');

/*-----------------------------------------
		INCLUDE ACF INTO RELEVANSSI EXCERPTS
-----------------------------------------*/
add_filter('relevanssi_excerpt_content', 'custom_fields_to_excerpts', 10, 3);
function custom_fields_to_excerpts($content, $post, $query) {

		/*
			Repeat the below code for each acf element you want to appear
			in the excerpt. Place them in the order you would want them
			to appear in the excerpt.

			There is no current way to include the repeater or flexiable
			content acf elements, keep this in mind when building pages.
		*/
		$custom_field = get_post_meta($post->ID, 'title', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'content_heading', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'location', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'sub-heading', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'address', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'city', true);
		$content .= " " . $custom_field;

		return $content;

}
?>