<?php

/*-----------------------------------------
	CUSTOM POST TYPES - www.wp-hasty.com
-----------------------------------------*/
// Post Type Key: attorney
function create_attorney_cpt() {
	$labels = array(
		'name' => __( 'Attorneys', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Attorney', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Attorneys', 'textdomain' ),
		'name_admin_bar' => __( 'Attorney', 'textdomain' ),
		'archives' => __( 'Attorney Archives', 'textdomain' ),
		'attributes' => __( 'Attorney Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Attorney:', 'textdomain' ),
		'all_items' => __( 'All Attorneys', 'textdomain' ),
		'add_new_item' => __( 'Add New Attorney', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Attorney', 'textdomain' ),
		'edit_item' => __( 'Edit Attorney', 'textdomain' ),
		'update_item' => __( 'Update Attorney', 'textdomain' ),
		'view_item' => __( 'View Attorney', 'textdomain' ),
		'view_items' => __( 'View Attorneys', 'textdomain' ),
		'search_items' => __( 'Search Attorney', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Attorney', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Attorney', 'textdomain' ),
		'items_list' => __( 'Attorneys list', 'textdomain' ),
		'items_list_navigation' => __( 'Attorneys list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Attorneys list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Attorney', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-businessman',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'attorney', $args );
}
add_action( 'init', 'create_attorney_cpt', 0 );
function posts_per_page_attorneys( $query ) {
  if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'attorney' ) ) {
		$query->set( 'posts_per_page', '16' );
		$query->set( 'orderby', 'title' );
		$query->set( 'order', 'ASC' );
  }
}
add_action( 'pre_get_posts', 'posts_per_page_attorneys' );
function template_chooser($template) {    
  global $wp_query;   
  $post_type = get_query_var('post_type');   
  if( $wp_query->is_search && $post_type == 'attorney' )   
  {
    return locate_template('search-archive.php');  //  redirect
  }   
  return $template;   
}
add_filter('template_include', 'template_chooser');   

// Post Type Key: practice
function create_practice_cpt() {
	$labels = array(
		'name' => __( 'Practices', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Practice', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Practices', 'textdomain' ),
		'name_admin_bar' => __( 'Practice', 'textdomain' ),
		'archives' => __( 'Practice Archives', 'textdomain' ),
		'attributes' => __( 'Practice Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Practice:', 'textdomain' ),
		'all_items' => __( 'All Practices', 'textdomain' ),
		'add_new_item' => __( 'Add New Practice', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Practice', 'textdomain' ),
		'edit_item' => __( 'Edit Practice', 'textdomain' ),
		'update_item' => __( 'Update Practice', 'textdomain' ),
		'view_item' => __( 'View Practice', 'textdomain' ),
		'view_items' => __( 'View Practices', 'textdomain' ),
		'search_items' => __( 'Search Practice', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Practice', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Practice', 'textdomain' ),
		'items_list' => __( 'Practices list', 'textdomain' ),
		'items_list_navigation' => __( 'Practices list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Practices list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Practice', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-awards',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'practice', $args );
}
add_action( 'init', 'create_practice_cpt', 0 );

// Post Type Key: testimony
function create_testimony_cpt() {
	$labels = array(
		'name' => __( 'Testimonies', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Testimony', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Testimonies', 'textdomain' ),
		'name_admin_bar' => __( 'Testimony', 'textdomain' ),
		'archives' => __( 'Testimony Archives', 'textdomain' ),
		'attributes' => __( 'Testimony Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Testimony:', 'textdomain' ),
		'all_items' => __( 'All Testimonies', 'textdomain' ),
		'add_new_item' => __( 'Add New Testimony', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Testimony', 'textdomain' ),
		'edit_item' => __( 'Edit Testimony', 'textdomain' ),
		'update_item' => __( 'Update Testimony', 'textdomain' ),
		'view_item' => __( 'View Testimony', 'textdomain' ),
		'view_items' => __( 'View Testimonies', 'textdomain' ),
		'search_items' => __( 'Search Testimony', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Testimony', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimony', 'textdomain' ),
		'items_list' => __( 'Testimonies list', 'textdomain' ),
		'items_list_navigation' => __( 'Testimonies list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Testimonies list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Testimony', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-format-quote',
		'supports' => array('title', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => false,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'testimony', $args );
}
add_action( 'init', 'create_testimony_cpt', 0 );

// Post Type Key: publication
function create_publication_cpt() {
	$labels = array(
		'name' => __( 'Publications', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Publication', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Publications', 'textdomain' ),
		'name_admin_bar' => __( 'Publication', 'textdomain' ),
		'archives' => __( 'Publication Archives', 'textdomain' ),
		'attributes' => __( 'Publication Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Publication:', 'textdomain' ),
		'all_items' => __( 'All Publications', 'textdomain' ),
		'add_new_item' => __( 'Add New Publication', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Publication', 'textdomain' ),
		'edit_item' => __( 'Edit Publication', 'textdomain' ),
		'update_item' => __( 'Update Publication', 'textdomain' ),
		'view_item' => __( 'View Publication', 'textdomain' ),
		'view_items' => __( 'View Publications', 'textdomain' ),
		'search_items' => __( 'Search Publication', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Publication', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Publication', 'textdomain' ),
		'items_list' => __( 'Publications list', 'textdomain' ),
		'items_list_navigation' => __( 'Publications list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Publications list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Publication', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-book-alt',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', ),
		'taxonomies' => array('law', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'publication', $args );
}
add_action( 'init', 'create_publication_cpt', 0 );
function posts_per_page_publications( $query ) {
  if ( !is_admin() && $query->is_main_query() && is_tax( 'law' ) ) {
		$query->set( 'posts_per_page', '6' );
		$query->set( 'orderby', 'date' );
		$query->set( 'order', 'DESC' );
  }
}
add_action( 'pre_get_posts', 'posts_per_page_publications' );

// Post Type Key: events
function create_events_cpt() {
	$labels = array(
		'name' => __( 'Event', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Events', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Event', 'textdomain' ),
		'name_admin_bar' => __( 'Events', 'textdomain' ),
		'archives' => __( 'Events Archives', 'textdomain' ),
		'attributes' => __( 'Events Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Events:', 'textdomain' ),
		'all_items' => __( 'All Event', 'textdomain' ),
		'add_new_item' => __( 'Add New Events', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Events', 'textdomain' ),
		'edit_item' => __( 'Edit Events', 'textdomain' ),
		'update_item' => __( 'Update Events', 'textdomain' ),
		'view_item' => __( 'View Events', 'textdomain' ),
		'view_items' => __( 'View Event', 'textdomain' ),
		'search_items' => __( 'Search Events', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Events', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Events', 'textdomain' ),
		'items_list' => __( 'Event list', 'textdomain' ),
		'items_list_navigation' => __( 'Event list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Event list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Events', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-calendar',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'events', $args );
}
add_action( 'init', 'create_events_cpt', 0 );

// Post Type Key: openposition
function create_openposition_cpt() {

	$labels = array(
		'name' => __( 'Open Positions', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Open Position', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Open Positions', 'textdomain' ),
		'name_admin_bar' => __( 'Open Position', 'textdomain' ),
		'archives' => __( 'Open Position Archives', 'textdomain' ),
		'attributes' => __( 'Open Position Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Open Position:', 'textdomain' ),
		'all_items' => __( 'All Open Positions', 'textdomain' ),
		'add_new_item' => __( 'Add New Open Position', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Open Position', 'textdomain' ),
		'edit_item' => __( 'Edit Open Position', 'textdomain' ),
		'update_item' => __( 'Update Open Position', 'textdomain' ),
		'view_item' => __( 'View Open Position', 'textdomain' ),
		'view_items' => __( 'View Open Positions', 'textdomain' ),
		'search_items' => __( 'Search Open Position', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Open Position', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Open Position', 'textdomain' ),
		'items_list' => __( 'Open Positions list', 'textdomain' ),
		'items_list_navigation' => __( 'Open Positions list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Open Positions list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Open Position', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-id-alt',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'openposition', $args );
}
add_action( 'init', 'create_openposition_cpt', 0 );

// Post Type Key: location
function create_location_cpt() {

	$labels = array(
		'name' => __( 'Locations', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Location', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Locations', 'textdomain' ),
		'name_admin_bar' => __( 'Location', 'textdomain' ),
		'archives' => __( 'Location Archives', 'textdomain' ),
		'attributes' => __( 'Location Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Location:', 'textdomain' ),
		'all_items' => __( 'All Locations', 'textdomain' ),
		'add_new_item' => __( 'Add New Location', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Location', 'textdomain' ),
		'edit_item' => __( 'Edit Location', 'textdomain' ),
		'update_item' => __( 'Update Location', 'textdomain' ),
		'view_item' => __( 'View Location', 'textdomain' ),
		'view_items' => __( 'View Locations', 'textdomain' ),
		'search_items' => __( 'Search Location', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Location', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Location', 'textdomain' ),
		'items_list' => __( 'Locations list', 'textdomain' ),
		'items_list_navigation' => __( 'Locations list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Locations list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Location', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-location-alt',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'location', $args );
}
add_action( 'init', 'create_location_cpt', 0 );