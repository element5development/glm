<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/posts/content', 'title'); ?>

<a id="content" class="anchor"></a>

<div class="hidden-overflow">

	<nav class="sectional-nav practice-nav">
		<ul>
			<li>
				<a href="<?php echo get_site_url(); ?>/practice/">
					Back to practices
				</a>
			</li>
		</ul>
	</nav>

	<?php if( !empty(get_the_content()) ) { ?>
		<section class="default-contents">
			<?php if ( get_field('title') ) { //POST TITLE ?>
				<h2><?php the_field('title'); ?></h2>
			<?php } else { ?>
				<h2><?php the_title(); ?></h2>
			<?php } ?>
			<?php get_template_part('template-parts/pages/content', 'default'); ?>
		</section>
	<?php } ?>

	<?php //QUERY ALL ATTORNEYS WITH THIS PRACTICE
		$posts = get_field('attorneys', false, false);
		$args = array(
			'post_type'      	=> 'attorney',
			'posts_per_page'	=> -1,
			'post__in'				=> $posts,
			'orderby'        	=> 'title',
			'order'						=> 'ASC' 
		);
		$attorney_query = new WP_Query( $args );
	?>

	<?php if ( $attorney_query->have_posts() ) : ?>
		<section class="attorney-feed feed default-contents">
		<h2>Practicing Attorneys</h2>
		<?php while ( $attorney_query->have_posts() ) : $attorney_query->the_post(); ?>
		<?php get_template_part( 'template-parts/posts/previews/preview-attorney' ); ?>
		<?php endwhile; ?>
		</section>
	<?php endif; ?>

</div>

<?php get_footer(); ?>