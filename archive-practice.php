<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/content', 'title'); ?>

<a id="content" class="anchor"></a>

<section class="default-contents">
	<p>Not only does Garan Lucow Miller P.C. have attorneys in offices located throughout Michigan and Indiana, we have attorneys grouped by multiple areas of practice to best serve your needs. <b>We know the law.</b> We're local. We know the community. We know clients want a law firm that represents them well.</p>
	<p><b>Use the slider below if you are looking for something in particular.</b></p>
	<form class="letter-filter">
		<label>A</label>
		<output id="display-letter"></output>
		<label>z</label>
		<input type="range" min="1" max="26" name="practice-letter" class="range-slider" id="practice-range">
	<form>
</section>

<section class="practice-feed feed default-contents">
		
		<?php //QUERY ALL PRACTICES
			$args = array( 
				'posts_per_page'  => -1, 
				'post_type' => 'practice',
				'orderby'=>'title',
				'order'=>'ASC'
				);
			$practices_query = new WP_Query( $args );
		?>
		<?php if ( $practices_query->have_posts() ) : ?>
			<?php 
				$last_post_letter = 'Z';
				$this_post_letter = 'A';
				$alphebet = range('A', 'Z');
			?>
			<?php while ( $practices_query->have_posts() ) : $practices_query->the_post(); ?>

				<?php	$this_post_letter = strtoupper( substr( $post->post_title, 0, 1 ) ); ?>

				<?php if ( $this_post_letter == $last_post_letter ) { ?>
					<!-- Nothing -->
				<?php } else { ?>
						<?php
							$last_post_value = array_search($last_post_letter, $alphebet);
							$this_post_value = array_search($this_post_letter, $alphebet);
							$differance = $this_post_value - $last_post_value;
							
							if ( $differance < 0 ) {
								echo '<h2>A</h2>';
								echo '<a id="A" class="letter-anchor"></a>';
							} else {
								$i = 1; 
								while( $i <= $differance ) {
									$position = $last_post_value + $i;
									echo '<h2>' . $alphebet[$position] . '</h2>';
									echo '<a id="' . $alphebet[$position] . '" class="letter-anchor"></a>';
									$i++;
								} 
							} 
						?>

				<?php } ?>

				<?php get_template_part( 'template-parts/posts/previews/preview-practice' ); ?>

				<?php $last_post_letter = $this_post_letter; ?>

			<?php endwhile; ?>
		<?php endif; ?>

</section>



<?php get_footer(); ?>
