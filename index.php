<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/content', 'title'); ?>

<a id="content" class="anchor"></a>

<div class="news-and-events">
	<section class="post-feed feed default-contents">
		<h2>News</h2>
		<?php if (!have_posts()) : ?>
			<p>Sorry, no results were found</p>
			<?php get_search_form(); ?>
		<?php endif; ?>
		
		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part( 'template-parts/posts/previews/preview', get_post_type() ); ?>
		<?php endwhile; ?>
	</section>
	<section id="sticky-events" class="event-feed default-contents">
		<h2>Events</h2>
		<?php //QUERY 3 EVENTS
			$args = array( 
				'posts_per_page'  => 3, 
				'post_type' => 'events',
				);
			$events_query = new WP_Query( $args );
		?>
		<?php if ( $events_query->have_posts() ) : ?>
			<?php while ( $events_query->have_posts() ) : $events_query->the_post(); ?>
				<?php get_template_part( 'template-parts/posts/previews/preview-events' ); ?>
			<?php endwhile; ?>
		<?php endif; ?>
		<?php //CHECK IF THERE ARE MORE EVENTS
			$count = $events_query->post_count;
		?>
		<?php if ( $count >= 3 ) : ?>
			<div class="event-cta">
				<a href="<?php echo get_site_url(); ?>/events" class="button">View All Events</a>
			</div>
		<?php else : ?>
			<div class="event-cta">
				<div class="block">
					<h2>More Events Coming Soon</h2>
					<p>Sign up for our newsletter to be the first to know!</p>
					<a href="<?php echo get_site_url(); ?>/events" class="button">Sign Up</a>
				</div>
			</div>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
	</section>
</div>

<div class="page-load-status">
	<p class="infinite-scroll-request">
		<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
			<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
				<animateTransform attributeType="xml"
					attributeName="transform"
					type="rotate"
					from="0 25 25"
					to="360 25 25"
					dur="0.6s"
					repeatCount="indefinite"/>
			</path>
		</svg>
	</p>
	<p class="infinite-scroll-last"></p>
	<p class="infinite-scroll-error">No more pages to load</p>
</div>

<?php
	the_posts_pagination( array(
		'prev_text'	=> __( 'Previous page' ),
		'next_text'	=> __( 'Next page' ),
	) );
?>

<div class="load-more-block">
	<button class="button load-more">Load more</button>
</div>

<?php get_template_part( 'template-parts/elements/newsletter' ); ?>

<?php get_template_part( 'template-parts/elements/best-law-firm' ); ?>

<?php get_footer(); ?>
