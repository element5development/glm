<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/content', 'title'); ?>

<section class="attorney-search-block">
	<div class="block">
		<svg class="attorney-search-close" viewBox="0 0 64 64" aria-labelledby="title" aria-describedby="desc">
			<path data-name="layer1" d="M53.122 48.88L36.243 32l16.878-16.878a3 3 0 0 0-4.242-4.242L32 27.758l-16.878-16.88a3 3 0 0 0-4.243 4.243l16.878 16.88-16.88 16.88a3 3 0 0 0 4.243 4.241L32 36.243l16.878 16.88a3 3 0 0 0 4.244-4.243z" ></path>
		</svg>
		<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
			<label>Attorney Search</label>
			<input type="search" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
			<input type="hidden" name="post_type" value="attorney" />
			<button type="submit">Search</button>
		</form>
	</div>
</section>

<a id="content" class="anchor"></a>

<div class="hidden-overflow">

	<nav class="attorney-nav sectional-nav">
		<?php 
			$args = array(
				'post_type'      => 'location',
				'posts_per_page' => -1,
				'order'          => 'ASC',
				'orderby'        => 'menu_order'
			);
			$child_pages = new WP_Query( $args );
		?>
		<?php if ( $child_pages->have_posts() ) : ?>
			<select name="attorney-location" id="attorney-locations" class="attorney-locations">
				<option value="-1">Filter by location</option>
				<?php while ( $child_pages->have_posts() ) : $child_pages->the_post(); ?>
					<option value="<?php the_permalink(); ?>"><?php the_title(); ?></option>
				<?php endwhile; ?>
			</select>
		<?php endif; wp_reset_postdata(); ?>
	</nav>

	<div class="design-heading-contain">
		<svg class="attorney-search-toggle" viewBox="0 0 64 64" aria-labelledby="title" aria-describedby="desc">
			<path data-name="layer1" d="M61.122 56.88L41.54 37.284l-.037.037a22.01 22.01 0 1 0-4.173 4.175l-.031.031L56.878 61.12a3 3 0 0 0 4.244-4.242zM24 40a16 16 0 1 1 16-16 16.002 16.002 0 0 1-16 16z"></path>
		</svg>
		<span class="design-heading rellax-heading">Trusted</span>
	</div>

	<section class="attorney-feed feed default-contents">
		<?php if (!have_posts()) : ?>
			<p>Sorry, no results were found</p>
			<?php get_search_form(); ?>
		<?php endif; ?>
		
		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part( 'template-parts/posts/previews/preview', get_post_type() ); ?>
		<?php endwhile; ?>
	</section>

	<div class="page-load-status">
		<p class="infinite-scroll-request">
			<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
				<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
					<animateTransform attributeType="xml"
						attributeName="transform"
						type="rotate"
						from="0 25 25"
						to="360 25 25"
						dur="0.6s"
						repeatCount="indefinite"/>
				</path>
			</svg>
		</p>
		<p class="infinite-scroll-last"></p>
		<p class="infinite-scroll-error">No more pages to load</p>
	</div>

	<?php
		the_posts_pagination( array(
			'prev_text'	=> __( 'Previous page' ),
			'next_text'	=> __( 'Next page' ),
		) );
	?>

	<div class="load-more-block">
		<button class="button load-more">Load more</button>
	</div>

</div>

<?php get_footer(); ?>
