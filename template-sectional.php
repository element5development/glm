<?php 
/*-------------------------------------------------------------------
    Template Name: Sectional
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/content', 'title'); ?>

<a id="content" class="anchor"></a>

<div class="hidden-overflow">

	<?php if( have_rows('sections') ): ?>
		<nav class="sectional-nav">
			<ul>
				<?php while ( have_rows('sections') ) : the_row(); ?>
					<li>
						<?php 
							$anchor = get_sub_field('section_title');
							$anchor = strtolower( get_sub_field('section_title') );
							$anchor = str_replace(' ', '-', $anchor);
						?>
						<a href="#<?php echo $anchor; ?>" class="smoothScroll">
							<?php the_sub_field('section_title'); ?>
						</a>
					</li>
				<?php endwhile; ?>
			</ul>
		</nav>
	<?php endif; ?>

	<?php if( !empty(get_the_content()) ) { ?>
		<section class="default-contents">
			<?php get_template_part('template-parts/pages/content', 'default'); ?>
		</section>
	<?php } ?>

	<?php if( have_rows('sections') ): ?>
		<div class="sections">
			<?php while ( have_rows('sections') ) : the_row(); ?>
				<?php 
					$anchor = get_sub_field('section_title');
					$anchor = strtolower( get_sub_field('section_title') );
					$anchor = str_replace(' ', '-', $anchor);
				?>
				<section class="section <?php echo $anchor; ?>">
					<a id="<?php echo $anchor; ?>" class="anchor"></a>
					<span class="design-heading rellax-heading"><?php the_sub_field('design_section_title') ?></span>
					<div class="content block">
						<h2><?php the_sub_field('section_title') ?></h2>
						<?php the_sub_field('content') ?>
					</div>
					<?php	$images = get_sub_field('images'); ?>
					<?php if( $images ): ?>
						<div class="gallery block">
							<?php foreach( $images as $image ): ?>
								<img class="rellax-image" src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</section>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>

</div>

<?php get_footer(); ?>