var $ = jQuery;

$(document).ready(function() {
  /*------------------------------------------------------------------
		Form input adding & removing classes
	------------------------------------------------------------------*/
  $('input:not([type=checkbox]):not([type=radio])').focus(function() {
    $(this).addClass('is-activated');
  });
  $('textarea').focus(function() {
    $(this).addClass('is-activated');
  });
  $('select').focus(function() {
    $(this).addClass('is-activated');
  });
  /*------------------------------------------------------------------
  	Click to Copy
  ------------------------------------------------------------------*/
  new Clipboard('.click-to-copy');
  $('.click-to-copy').on('click', function() {
    $(this).addClass('copied');
    $this = $(this);
    setTimeout(function() {
      $this.removeClass('copied');
    }, 2000);
  });
  /*------------------------------------------------------------------
  	Element5 Site Credit Appear
  ------------------------------------------------------------------*/
  $('#element5-credit img').viewportChecker({
    classToAdd: 'visible',
    offset: 10,
    repeat: true,
  });
  /*------------------------------------------------------------------
  	Sticky Primary Navigation
	------------------------------------------------------------------*/
  $(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll > 0) {
      $('.primary-nav-container').addClass('is_stuck');
    } else {
      $('.primary-nav-container').removeClass('is_stuck');
    }
  });
  /*------------------------------------------------------------------
  	TOGGLE MOBILE NAVIGATION
	------------------------------------------------------------------*/
  $('.menu').on('click', function() {
    $('.primary-nav-container').toggleClass('open');
    $('body').toggleClass('no-scroll');
  });
  /*------------------------------------------------------------------
  	TOGGLE PRIMARY SEARCH FORM
	------------------------------------------------------------------*/
  $('.search-toggle').on('click', function() {
    $('.search-block').addClass('active');
    $('.search-block').css('z-index', '11000');
    $('body').addClass('no-scroll');
  });
  $('.search-close').on('click', function() {
    $('.search-block').removeClass('active');
    setTimeout(function() {
      $('.search-block').css('z-index', '-1');
    }, 500);
    $('body').removeClass('no-scroll');
  });
  /*------------------------------------------------------------------
  	TOGGLE ATTORNEY SEARCH FORM
	------------------------------------------------------------------*/
  $('.attorney-search-toggle').on('click', function() {
    $('.attorney-search-block').addClass('active');
    $('.attorney-search-block').css('z-index', '11000');
    $('body').addClass('no-scroll');
  });
  $('.attorney-search-close').on('click', function() {
    $('.attorney-search-block').removeClass('active');
    setTimeout(function() {
      $('.attorney-search-block').css('z-index', '-1');
    }, 500);
    $('body').removeClass('no-scroll');
  });
  /*------------------------------------------------------------------
  	PARALLAX SECTIONS
	------------------------------------------------------------------*/
  if ($(window).width() > 1023) {
    if ($('body').hasClass('page-template-template-sectional') || $('body').hasClass('page-template-template-homepage') || $('body').hasClass('single-openposition')) {
      var rellaxImage = new Rellax('.rellax-image', {
        speed: 4,
        center: true,
      });
    }
    if ($('body').hasClass('page-template-template-sectional') || $('body').hasClass('page-template-template-homepage') || $('body').hasClass('single-openposition') || $('body').hasClass('post-type-archive-attorney')) {
      var rellaxHeading = new Rellax('.rellax-heading', {
        speed: 2,
        center: true,
      });
    }
  }
  /*------------------------------------------------------------------
  	Logic to display load more button
	------------------------------------------------------------------*/
  if ($('.next.page-numbers').length) {
    $('.load-more').css('display', 'block');
  }
  /*------------------------------------------------------------------
  	Infinite Scroll Init on Default Blog
  ------------------------------------------------------------------*/
  $('.feed').infiniteScroll({
    path: '.next.page-numbers',
    append: '.post-preview',
    button: '.load-more',
    scrollThreshold: false,
    checkLastPage: true,
    status: '.page-load-status',
  });
  /*------------------------------------------------------------------
  	GOOGLE MAPS
	------------------------------------------------------------------*/
  $('.acf-map').each(function() {
    map = new_map($(this));
  });

  function new_map($el) {
    var $markers = $el.find('.marker');
    var args = {
      zoom: 16,
      center: new google.maps.LatLng(0, 0),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map($el[0], args);
    map.markers = [];
    $markers.each(function() {
      add_marker($(this), map);
    });
    center_map(map);
    return map;
  }

  function add_marker($marker, map) {
    var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
    var marker = new google.maps.Marker({
      position: latlng,
      map: map
    });
    map.markers.push(marker);
    if ($marker.html()) {
      var infowindow = new google.maps.InfoWindow({
        content: $marker.html()
      });
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
      });
    }
  }

  function center_map(map) {
    var bounds = new google.maps.LatLngBounds();
    $.each(map.markers, function(i, marker) {
      var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
      bounds.extend(latlng);
    });
    if (map.markers.length == 1) {
      map.setCenter(bounds.getCenter());
      map.setZoom(16);
    } else {
      map.fitBounds(bounds);
    }
  }

  /*------------------------------------------------------------------
  	PRACTICE RANGE SLIDER
	------------------------------------------------------------------*/
  if ($('body').hasClass('post-type-archive-practice')) {
    $range = document.getElementById('practice-range');
    $output = document.getElementById('display-letter');
    $baseUrl = document.location.origin;
    $letters = [], i = 'A'.charCodeAt(0), j = 'Z'.charCodeAt(0);
    for (; i <= j; ++i) {
      $letters.push(String.fromCharCode(i));
    }

    // DISPLAY LETTER 
    $range.oninput = function() {
      $output.innerHTML = $letters[($range.value - 1)];
    }

    // LINK TO ANCHOR
    function onRangeChange() {
      if ($('practice-range').val() != -1) {
        $rangeValue = ($('#practice-range').val() - 1);
        $locationID = '#' + $letters[$rangeValue];
        $('html, body').animate({
          scrollTop: $($locationID).offset().top
        }, 1000);
      }
    }
    $range.onchange = onRangeChange;
  }
  /*------------------------------------------------------------------
  	TESTIMONY SLIDER
	------------------------------------------------------------------*/
  $('.testimony-sldier').slick({
    centerMode: true,
    centerPadding: '150px',
    slidesToShow: 3,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [{
        breakpoint: 1024,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });

  /*------------------------------------------------------------------
  	ATTORNEY LOCATION FILTER
	------------------------------------------------------------------*/
  if ($('body').hasClass('post-type-archive-attorney') || $('body').hasClass('post-type-archive-attorney')) {
    $dropdown_two = document.getElementById("attorney-locations");
    $baseUrl = document.location.origin;

    function onFilterChange() {
      if ($dropdown_two.options[$dropdown_two.selectedIndex].value != -1) {
        location.href = $baseUrl + $dropdown_two.options[$dropdown_two.selectedIndex].value + "/#attorney-by-location";
      }
    }
    $dropdown_two.onchange = onFilterChange;
  }

  /*------------------------------------------------------------------
  	ATTORNEY STICKY BAR
	------------------------------------------------------------------*/
  if ($('body').hasClass('single-attorney')) {
    new PrettyScroll('.attorney-sidebar-inner', {
      container: '.sidebar-and-content',
      breakpoint: 700,
      offsetTop: 95,
      offsetBottom: 95,
    });
  }

  /*------------------------------------------------------------------
  	PUBLICATION STICKY BAR
	------------------------------------------------------------------*/
  if ($('body').hasClass('single-publication')) {
    new PrettyScroll('.publication-sidebar-inner', {
      container: '.sidebar-and-content',
      breakpoint: 700,
      offsetTop: 95,
      offsetBottom: 95,
    });
  }

  /*------------------------------------------------------------------
  	BACK TO TOP
	------------------------------------------------------------------*/
  window.onscroll = function() { scrollFunction() };

  function scrollFunction() {
    if (document.body.scrollTop > 650 || document.documentElement.scrollTop > 650) {
      $('.back-to-top').addClass('visible');
    } else {
      $('.back-to-top').removeClass('visible');
    }
  }


});