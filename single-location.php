<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>
<?php get_header(); ?>

<?php get_template_part('template-parts/posts/content', 'title'); ?>

<a id="content" class="anchor"></a>

<section class="location-meta">
	<div class="meta">
		<h2>
			<?php the_field('city'); ?> Location</h2>
		<p>
			<address>
				<?php the_field('address'); ?>
				<?php the_field('address_2'); ?>
				<?php the_field('city'); ?>,
				<?php the_field('state'); ?>
				<?php the_field('zip'); ?>
			</address>
		</p>
		<?php 
			$phoneOne = preg_replace('/[^0-9]/', '', get_field('phone_one'));
			$phoneTwo = preg_replace('/[^0-9]/', '', get_field('phone_two'));
		?>
		<p>
			<a href="tel:+1<?php echo $phoneOne; ?>">
				<?php the_field('phone_one'); ?>
			</a> |
			<a href="tel:+1<?php echo $phoneTwo; ?>">
				<?php the_field('phone_two'); ?>
			</a>
		</p>
	</div>
	<?php $posts = get_field('host'); ?>
	<?php if( $posts ): ?>
	<div class="hosts">
		<h2>Managing Partner</h2>
		<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
		<?php setup_postdata($post); ?>
		<div class="host">
			<?php if ( get_field('title') ) { //POST TITLE ?>
			<a href="<?php the_permalink(); ?>">
				<?php the_field('title'); ?>
			</a>
			<?php } else { ?>
			<h3>
				<?php the_title(); ?>
				<span>,</span>
			</h3>
			<?php } ?>
		</div>
		<?php endforeach; ?>
		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	</div>
	<?php endif; ?>
</section>

<section class="default-contents">
	<?php if( !empty(get_the_content()) ) { ?>
	<h2>About this branch</h2>
	<?php get_template_part('template-parts/pages/content', 'default'); ?>
	<?php } ?>
	<?php $location = get_field('map'); ?>
	<?php if( !empty($location) ): ?>
	<div class="acf-map">
		<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
	</div>
	<?php endif; ?>
</section>

<?php //QUERY ALL ATTORNEYS WITH THIS PRACTICE
	$args = array( 
		'posts_per_page'  => -1, 
		'post_type' => 'attorney',
		'orderby' => 'title',
		'order' => 'ASC',
		'meta_query' => array(
	    array(
	        'key' => 'location',
	        'value' => get_the_ID()
	    ),
    )
	);
	$attorney_query = new WP_Query( $args );
?>
<?php if ( $attorney_query->have_posts() ) : ?>
<section class="attorney-feed feed default-contents">
	<a id="attorney-by-location" class="anchor"></a>
	<h2>Attorneys at thE
		<?php the_field('city'); ?> location</h2>
	<?php while ( $attorney_query->have_posts() ) : $attorney_query->the_post(); ?>
	<?php get_template_part( 'template-parts/posts/previews/preview-attorney' ); ?>
	<?php endwhile; ?>
</section>
<?php endif; ?>
<?php wp_reset_query(); ?>

<?php get_footer(); ?>